# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.2.0](https://gitlab.com/doertydoerk/gitlabmon/compare/v2.0.1...v2.2.0) (2023-04-17)


### Features

* **UI:** add version and build number to pipelines view ([0aaaf01](https://gitlab.com/doertydoerk/gitlabmon/commit/0aaaf01b889a0a097764a041c4640fc6c6301fd7))


### Bug Fixes

* **artifact:** notarization app as zip ([53c5f1d](https://gitlab.com/doertydoerk/gitlabmon/commit/53c5f1dd8895645dda4a8f7cabc5db62add36470))
* **artifact:** staple notarization to app ([7c6c3ec](https://gitlab.com/doertydoerk/gitlabmon/commit/7c6c3ec33635b2fff5a3b7888e40b702239b6109))
* **readme:** binary link ([29bbab8](https://gitlab.com/doertydoerk/gitlabmon/commit/29bbab8c0c8b80dd5815fb3a3b240af1deac0881))
* **release.sh:** update ([eb41bc2](https://gitlab.com/doertydoerk/gitlabmon/commit/eb41bc235298cedbae65c20b7ca4fc6824caba43))
* **trafficLight:** state not kept ([9866dc3](https://gitlab.com/doertydoerk/gitlabmon/commit/9866dc37c37dc5ca53eadfa650d5525187945379))

## [2.1.0](https://gitlab.com/doertydoerk/gitlabmon/compare/v2.0.1...v2.1.0) (2023-04-17)


### Features

* **UI:** add version and build number to pipelines view ([0aaaf01](https://gitlab.com/doertydoerk/gitlabmon/commit/0aaaf01b889a0a097764a041c4640fc6c6301fd7))


### Bug Fixes

* **artifact:** notarization app as zip ([53c5f1d](https://gitlab.com/doertydoerk/gitlabmon/commit/53c5f1dd8895645dda4a8f7cabc5db62add36470))
* **artifact:** staple notarization to app ([7c6c3ec](https://gitlab.com/doertydoerk/gitlabmon/commit/7c6c3ec33635b2fff5a3b7888e40b702239b6109))
* **readme:** binary link ([29bbab8](https://gitlab.com/doertydoerk/gitlabmon/commit/29bbab8c0c8b80dd5815fb3a3b240af1deac0881))
* **release.sh:** update ([eb41bc2](https://gitlab.com/doertydoerk/gitlabmon/commit/eb41bc235298cedbae65c20b7ca4fc6824caba43))
* **trafficLight:** state not kept ([9866dc3](https://gitlab.com/doertydoerk/gitlabmon/commit/9866dc37c37dc5ca53eadfa650d5525187945379))

### [2.0.1](https://gitlab.com/doertydoerk/gitlabmon/compare/v2.0.0...v2.0.1) (2023-04-09)

## 2.0.0 (2023-04-05)


### ⚠ BREAKING CHANGES

* **ui:** overhaul

### Features

* **accounts:** Indicate network error for selected account ([af0a25b](https://gitlab.com/doertydoerk/gitlabmon/commit/af0a25b89e5e3b2adb0c36b64a5d4a48d1b6ed01))
* **multipleAccounts:** update model & UI for multiple accounts ([1d67ef8](https://gitlab.com/doertydoerk/gitlabmon/commit/1d67ef8548130eef4f8c039e87b32d00d6f4641a))


### Bug Fixes

* **accountsDropdown:** behaviour ([6feee4f](https://gitlab.com/doertydoerk/gitlabmon/commit/6feee4f3b1dfcce4324a9ebd5bac80c08d6fc9ac))
* **accountsDropdown:** behaviour on last account removed ([8efa29f](https://gitlab.com/doertydoerk/gitlabmon/commit/8efa29fcf296eba13dfa93e8e7c3b3d130b48b0d))
* **CiClient:** comment code line back in ([0aff551](https://gitlab.com/doertydoerk/gitlabmon/commit/0aff5512846039de6be86cb079411dabeb9f22a3))
* **ci:** DebugConfig ([ed6081d](https://gitlab.com/doertydoerk/gitlabmon/commit/ed6081d235fce26121ec65b4f171b2ba562ea685))
* **credentials:** accounts with invalid credentials behaviour ([4da5063](https://gitlab.com/doertydoerk/gitlabmon/commit/4da506349297ed4318cc6188f2f20bba4a1d4b9f))
* **credentials:** edit button brings up wrong account ([9d0385f](https://gitlab.com/doertydoerk/gitlabmon/commit/9d0385f7aefffc6c3f334cbdd2d7688d969837b3))
* **credentialsView:** selected account is shown on tops of menu ([ed76e21](https://gitlab.com/doertydoerk/gitlabmon/commit/ed76e21d4adfe7047872bf1b4704bedd49e0069f))
* display projects with more than 2 items correctly ([ed82213](https://gitlab.com/doertydoerk/gitlabmon/commit/ed82213d3cbb5f3cf8dc5550560476392273a21b))
* obsolote starred projects still showing ([fb973e1](https://gitlab.com/doertydoerk/gitlabmon/commit/fb973e103f886e1e778ef7662a141a9efafc9447))
* **PreferencesView:** loading spinner going forever on network failure ([e4898bd](https://gitlab.com/doertydoerk/gitlabmon/commit/e4898bd393e82bb21f11bb787d09882c45759ace))
* **UI:** remove incorrect tool tip ([0ded167](https://gitlab.com/doertydoerk/gitlabmon/commit/0ded167c41afd5d50634ff47e7c60f95702da75e))


### refac

* **ui:** overhaul ([9c8eddd](https://gitlab.com/doertydoerk/gitlabmon/commit/9c8eddd7cbf50c77afb3358101ade0b659ff79e9))
