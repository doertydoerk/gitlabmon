@testable import Gitlab_Monitor
import XCTest

final class DurattionTests: XCTestCase {
    func testSecondsToFormattedDureation() throws {
        // < 1 Minute
        XCTAssertEqual("< 1m", Duration.secondsToFormatted(seconds: Duration.oneSecond * 59))

        // minutes
        XCTAssertEqual("5m", Duration.secondsToFormatted(seconds: Duration.oneSecond * 300))
        XCTAssertEqual("5m", Duration.secondsToFormatted(seconds: Duration.oneSecond * 320))
        XCTAssertEqual("6m", Duration.secondsToFormatted(seconds: Duration.oneSecond * 360))
        XCTAssertEqual("1h", Duration.secondsToFormatted(seconds: Duration.oneSecond * 3601))

        // hours
        XCTAssertEqual("1h", Duration.secondsToFormatted(seconds: Duration.oneHour))
        XCTAssertEqual("23h", Duration.secondsToFormatted(seconds: Duration.oneDay - 1))
        XCTAssertEqual("1d", Duration.secondsToFormatted(seconds: Duration.oneDay + 1))

        // days
        XCTAssertEqual("1d", Duration.secondsToFormatted(seconds: Duration.oneDay))
        XCTAssertEqual("6d", Duration.secondsToFormatted(seconds: Duration.oneDay * 6))
        XCTAssertEqual("1w", Duration.secondsToFormatted(seconds: Duration.oneDay * 7))

        // weeks
        XCTAssertEqual("1w", Duration.secondsToFormatted(seconds: Duration.oneWeek))
        XCTAssertEqual("3w", Duration.secondsToFormatted(seconds: Duration.oneWeek * 3))
        XCTAssertEqual("1m", Duration.secondsToFormatted(seconds: Duration.oneWeek * 4))

        // months
        XCTAssertEqual("1m", Duration.secondsToFormatted(seconds: Duration.oneMonth))
        XCTAssertEqual("11m", Duration.secondsToFormatted(seconds: Duration.oneMonth * 11))
        XCTAssertEqual("> 1y", Duration.secondsToFormatted(seconds: Duration.oneMonth * 12))

        // year
        XCTAssertEqual("> 1y", Duration.secondsToFormatted(seconds: Duration.oneYear))
    }
}
