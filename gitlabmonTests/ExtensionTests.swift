@testable import Gitlab_Monitor
import XCTest

class ExtensionTests: XCTestCase {
    func testMergeDictionary() throws {
        var dict1: [String: [String]] = [
            "dict1_key1": ["dict1_value_1"],
        ]

        let dict2: [String: [String]] = [
            "dict2_key2": ["dict2_value_2"],
        ]

        dict1.merge(dict: dict2)
        XCTAssertEqual(2, dict1.count)
        XCTAssertEqual("dict2_value_2", dict1["dict2_key2"]![0])
    }
}
