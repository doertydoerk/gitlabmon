@testable import Gitlab_Monitor
import XCTest

class AccountsServiceTests: XCTestCase {
    var accountsService: AccountsRepository!
    let testUrl = "test_url"
    let testUserId = "test_id"
    let testToken = "test_token"
    var defaults: UserDefaults! = UserDefaults(suiteName: "unit_tests")

    func testAccounts() {
        accountsService = AccountsRepository(withUserDefault: defaults)
        accountsService.resetDefaults()
        XCTAssertEqual(accountsService.accountsCount(), 0)

        // create account
        let cred = Credentials(url: testUrl, userId: testUserId, token: testToken)
        let account = GitlabAccount(credentials: cred)
        XCTAssertNotNil(account)

        // create accounts dictionary
        accountsService.addAccount(account: account)
        XCTAssertEqual(1, accountsService.accountsCount())

        // read account from dictionary
        var actual = accountsService.fetchAccounts()!
        XCTAssertEqual(actual[account.id]?.credentials?.url, cred.url)
        XCTAssertEqual(actual[account.id]?.credentials?.userId, cred.userId)
        XCTAssertEqual(actual[account.id]?.credentials?.token, cred.token)

        XCTAssertEqual(actual[account.id]?.preferences?.refreshDelay, "120")
        XCTAssertEqual(actual[account.id]?.preferences?.projectType, "owned")
        XCTAssertEqual(actual[account.id]?.preferences?.useAmpel, false)

        XCTAssertNotNil(actual[account.id]?.projects?.count)

        // update account
        actual[account.id]?.preferences?.useAmpel = true
        accountsService.addAccount(account: actual[account.id]!)

        // read upadated
        actual = accountsService.fetchAccounts()!
        XCTAssertNotNil(account)

        XCTAssertEqual(actual[account.id]?.credentials?.url, cred.url)
        XCTAssertEqual(actual[account.id]?.credentials?.userId, cred.userId)
        XCTAssertEqual(actual[account.id]?.credentials?.token, cred.token)

        XCTAssertEqual(actual[account.id]?.preferences?.refreshDelay, "120")
        XCTAssertEqual(actual[account.id]?.preferences?.projectType, "owned")
        XCTAssertEqual(actual[account.id]?.preferences?.useAmpel, true)

        XCTAssertNotNil(actual[account.id]?.projects?.count)

        // clean up
        accountsService.resetDefaults()
    }
}
