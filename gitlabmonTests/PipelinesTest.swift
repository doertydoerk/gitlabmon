@testable import Gitlab_Monitor
import XCTest

class PipelinesTest: XCTestCase {
    func testGetPipelineName_regular() throws {
        let pipe = Pipeline(updatedAt: "date_1",
                            branch: "bar_1",
                            status: .success,
                            webUrl: "https://gitcom/aoe/go-shared/-/pipelines/34352",
                            hash: "123ABC")

        XCTAssertEqual("aoe / go-shared", pipe.getProjectName())
    }

    func testGetPipelineName_dummy() throws {
        let pipe = Pipeline(updatedAt: "date_1",
                            branch: "bar_1",
                            status: .success,
                            webUrl: "https://git.com/some%20project%20had%20no%20proper%20pipeline/-",
                            hash: "123ABC")

        XCTAssertEqual("some project had no proper pipeline", pipe.getProjectName())
    }

    func testTimeElapsed() {
        let date = Calendar.current.date(byAdding: .day, value: -7, to: Date())
        let p = Pipeline(updatedAt: date!.nowRfc3339(),
                         branch: "",
                         status: .success,
                         webUrl: "",
                         hash: "123ABC")
        XCTAssertEqual("1w", p.timeSinceLastUpdate())
    }
}
