@testable import Gitlab_Monitor
import XCTest

class ProjectsTests: XCTestCase {
    let accountService = AccountsRepository()

    func testMergeNew() throws {
        accountService.updateAccount(projects: Projects())
        let project_1 = Project(id: 1, displayName: "name_1", subscribed: false)
        let project_2 = Project(id: 2, displayName: "name_2", subscribed: true)
        let project_3 = Project(id: 3, displayName: "name_3", subscribed: true)
        var old = Projects(arrayLiteral: project_1, project_2, project_3)
        accountService.updateAccount(projects: old)
        XCTAssertEqual(3, old.count)
        XCTAssertFalse(old[0].subscribed)

        // we expect the subscription being the same as before. It should not be overwritten.
        let project_4 = Project(id: 1, displayName: "name_1", subscribed: true)
        let project_5 = Project(id: 5, displayName: "name_5", subscribed: true)
        let new = Projects(arrayLiteral: project_4, project_5)

        let updated = old.update(newProjects: new)
        XCTAssertEqual(2, updated.count)
        // we wnat this to be false as we already said that we don't want to subscribe to this one on last update
        XCTAssertFalse(updated[0].subscribed)

        accountService.updateAccount(projects: Projects())
    }
}
