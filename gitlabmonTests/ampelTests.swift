@testable import Gitlab_Monitor
import XCTest

class ampelTests: XCTestCase {
    let ampel = AmpelClient()

    func testSwitchAmpel() {
        let time: uint = 1
        ampel.switchAmpel(status: .allOff)
        sleep(time)
        ampel.switchAmpel(status: .green)
        sleep(time)
        ampel.switchAmpel(status: .red)
        sleep(time)
        ampel.switchAmpel(status: .allOn)
        sleep(time)
        ampel.switchAmpel(status: .allOff)
        sleep(time)
    }
}
