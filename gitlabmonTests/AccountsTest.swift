@testable import Gitlab_Monitor
import XCTest

class AccountsTest: XCTestCase {
    let url = "https://gitlab.com"
    let userId = "42"
    let token = "test_token"
    var account: GitlabAccount!

    override func setUpWithError() throws {
        account = createAccount()
    }

    override func tearDownWithError() throws {
        // Put tear down code here. This method is called after the invocation of each test method in the class.
    }

    func createAccount() -> GitlabAccount {
        let cred = Credentials(url: url, userId: userId, token: token)
        let account = GitlabAccount(credentials: cred)
        XCTAssertEqual(account.credentials?.url, url)
        XCTAssertEqual(account.credentials?.userId, userId)
        XCTAssertEqual(account.credentials?.token, token)
        XCTAssertEqual(account.preferences?.refreshDelay, "120")
        XCTAssertEqual(account.preferences?.projectType, "owned")
        XCTAssertEqual(account.preferences?.useAmpel, false)
        XCTAssertEqual(0, account.projects?.count)
        return account
    }

    func testGetName() throws {
        XCTAssertEqual(account.getName(), "gitlab.com")
    }

    func testGetIdentifier() {
        XCTAssertNotNil(UUID(uuidString: account.getID()))
    }

    func testFetchProjects() {
        let project = Project(id: 0, displayName: "first_project", subscribed: true)
        let projects = [project]
        account.projects = projects
        let actual = account.fetchProjects()
        XCTAssertEqual(1, actual?.count)
    }

    func testFetchCredentials() {
        let actual = account.fetchCredentials()
        XCTAssertNotNil(actual)
        XCTAssertEqual(actual?.url, url)
        XCTAssertEqual(actual?.userId, userId)
        XCTAssertEqual(actual?.token, token)
    }

    func testUpdateCredentials() {
        let newToken = "new_token"
        let old = (account.fetchCredentials())!
        let new = Credentials(url: old.url, userId: old.userId, token: newToken)
        account.update(credentials: new)
        XCTAssertEqual(newToken, account.fetchCredentials()?.token)
    }

    func testFetchPreferences() {
        let actual = account.fetchPreferences()
        XCTAssertNotNil(actual)
        XCTAssertEqual(actual.refreshDelay, "120")
        XCTAssertEqual(actual.projectType, "owned")
        XCTAssertEqual(actual.useAmpel, false)
    }

    func testUpdatePreferences() {
        let new = Preferences(refreshDelay: "60", projectType: "starred", useAmpel: true)
        account.update(preferences: new)
        XCTAssertEqual(new.refreshDelay, account.fetchPreferences().refreshDelay)
        XCTAssertEqual(new.projectType, account.fetchPreferences().projectType)
        XCTAssertEqual(new.useAmpel, account.fetchPreferences().useAmpel)
    }

    func testUpdateProjects() {
        let project = Project(id: 0, displayName: "first_project", subscribed: true)
        let projects = [project]
        account.update(projects: projects)
        XCTAssertEqual(1, account.fetchProjects()?.count)
    }
}
