import Cocoa

class PreferencesTableCell: NSTableCellView {
    @IBOutlet var checkBox: NSButton!
    @IBOutlet var projectName: NSTextField!
    weak var controller: PreferencesViewController?
    private let accountService = AccountsRepository.shared
    var projectId: Int!

    @IBAction func checkBoxClicked(_ sender: NSButton) {
        if var projects = accountService.getProjects() {
            if sender.state == .on {
                projects.subscribeProject(projectId: projectId)
            } else {
                projects.unsubscribeProject(projectId: projectId)
            }

            accountService.updateAccount(projects: projects)
            controller?.projects = accountService.getProjects()!
        }
    }
}

// Used for check boxes only
extension NSButton {
    func markedCheck(checked: Bool) {
        if checked {
            state = .on
        } else {
            state = .off
        }
    }
}
