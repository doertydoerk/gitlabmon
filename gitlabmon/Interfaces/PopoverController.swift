import Cocoa
import RealEventsBus

class PopoverController {
    enum indicatorColor: String {
        case grey = "greyBranch"
        case red = "redBranch"
        case green = "greenBranch"
    }

    private let ampel = AmpelClient()
    private var eventMonitor: EventMonitor?
    private let statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.squareLength)
    private let popover = NSPopover()
    private let preferencesViewController: PreferencesViewController?
    private let pipelineStatusViewController: PipelineStatusViewController?

    // TODO: - insert CI Client here
    // let monitorService = MonitorService(ciClient: MockClient())
    let monitorService = MonitorService(ciClient: GitlabClient())

    init() {
        ampel.switchAmpel(status: .allOff)
        preferencesViewController = NSStoryboard(name: "Main", bundle: nil).instantiateController(withIdentifier: "PreferencesViewControllerID") as? PreferencesViewController
        preferencesViewController?.monitorService = monitorService

        pipelineStatusViewController = NSStoryboard(name: "Main", bundle: nil).instantiateController(withIdentifier: "PopoverViewControllerID") as? PipelineStatusViewController
        popover.contentViewController = pipelineStatusViewController
        pipelineStatusViewController?.monitorService = monitorService

        if let button = statusItem.button {
            setStatusColor(color: indicatorColor.grey)
            button.target = self
            button.sendAction(on: [.leftMouseUp, .rightMouseUp])
            button.action = #selector(statusBarButtonClicked(_:))
            button.toolTip = "Gitlab Monitor"
        }

        monitorService.delegate = self
        configureEventMonitor()
        registerEvents()
    }

    deinit {
        Bus<AppEvents>.unregister(self)
    }

    @objc func statusBarButtonClicked(_: AnyObject?) {
        let event = NSApp.currentEvent!

        if event.type == NSEvent.EventType.leftMouseUp {
            if popover.contentViewController == pipelineStatusViewController {
                toggleView()
            } else {
                closePopover()
                popover.contentViewController = pipelineStatusViewController
                popover.contentSize.width = 500
                showPopover()
            }
        }

        if event.type == NSEvent.EventType.rightMouseUp {
            if popover.contentViewController == preferencesViewController {
                toggleView()
            } else {
                closePopover()
                popover.contentViewController = preferencesViewController
                popover.contentSize.width = 350
                showPopover()
            }
        }
    }

    func toggleView() {
        if popover.isShown {
            closePopover()
        } else {
            showPopover()
        }
    }

    // TODO: this may be redundant
    func updateStatusIcon(status: PipelineStatus) {
        switch status {
        case .failed:
            setStatusColor(color: indicatorColor.red)
        case .success:
            setStatusColor(color: indicatorColor.green)
        default:
            setStatusColor(color: indicatorColor.grey)
        }
    }

    private func setStatusColor(color: indicatorColor) {
        DispatchQueue.main.async {
            self.statusItem.button?.image = NSImage(named: color.rawValue)
        }
    }

    private func showPopover() {
        NSApp.activate(ignoringOtherApps: true)
        if let button = statusItem.button {
            popover.show(relativeTo: button.bounds, of: button, preferredEdge: NSRectEdge.minY)
        }
        eventMonitor?.start()
    }

    private func closePopover() {
        preferencesViewController?.view.window?.close()
        popover.performClose(nil)
        popover.contentViewController = nil
        eventMonitor?.stop()
    }

    // allows to close the popover's upon outside click
    private func configureEventMonitor() {
        eventMonitor = EventMonitor(mask: [.leftMouseDown, .rightMouseDown]) { [weak self] _ in
            if let strongSelf = self, strongSelf.popover.isShown {
                strongSelf.closePopover()
            }
        }
    }

    func registerEvents() {
        Bus<AppEvents>.register(self) { event in
            switch event {
            case .accountsDidReset:
                self.setStatusColor(color: indicatorColor.grey)
            case .networkError:
                self.setStatusColor(color: indicatorColor.grey)
            default: break
            }
        }
    }
}

extension PopoverController: MonitorServiceDelegate {
    func projectsDidUpdate(projects: [Project]) {
        preferencesViewController?.projects = projects
    }

    func pipelinesDidUpdate(pipelines: Pipelines) {
        switch pipelines.hasFailedPipeline() {
        case true:
            setStatusColor(color: .red)
            ampel.switchAmpel(status: .red)
        default:
            if pipelines.count > 0 {
                setStatusColor(color: .green)
                ampel.switchAmpel(status: .green)
            } else {
                setStatusColor(color: .grey)
                ampel.switchAmpel(status: .allOff)
            }
        }

        pipelineStatusViewController?.pipelines = pipelines.sortByAge()
    }
}
