import Cocoa
import RealEventsBus

class PreferencesViewController: NSViewController {
    let editAccountSegueKey = "editAccountSegue"
    let addAccountSegueKey = "addAccountSegue"

    @IBOutlet var tableView: NSTableView!
    @IBOutlet var refreshButton: NSButton!
    @IBOutlet var refreshDelayButton: NSPopUpButton!
    @IBOutlet var projectTypeButton: NSPopUpButton!
    @IBOutlet var useTrafficLight: NSButton!
    @IBOutlet var loadingIndicator: NSProgressIndicator!
    @IBOutlet var accountsDropdown: NSPopUpButton!

    var accountsService = AccountsRepository.shared
    weak var monitorService: MonitorService?
    weak var credentialsViewController: CredentialsViewController?

    var projects: [Project]? {
        didSet {
            loadingIndicator(show: false) // TODO: move to notification handler

            guard let count = projects?.count else {
                return
            }

            if count > 0 {
                DispatchQueue.main.async {
                    self.tableView?.reloadData()
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // accountsService.resetDefaults()
        registerEvents()
        tableView.delegate = self
        tableView.dataSource = self as NSTableViewDataSource
    }

    override func viewWillAppear() {
        super.viewWillAppear()

        populatePreferences()
        initAccountsPopUp()

        if let projects = accountsService.fetchAccount(id: accountsService.currentlyUsedAccountID)?.fetchProjects() {
            self.projects = projects

            if let useAmpel = (accountsService.getPreferences()?.useAmpel) {
                if useAmpel {
                    useTrafficLight.state = .on
                } else {
                    useTrafficLight.state = .off
                }
            }
        }

        if AppearanceHelper.isLight() {
            tableView.backgroundColor = .lightGray
        } else {
            tableView.backgroundColor = .darkGray
        }

        updateView()
    }

    deinit {
        Bus<AppEvents>.unregister(self)
    }

    fileprivate func initAccountsPopUp() {
        accountsDropdown.removeAllItems()
        if let accounts = accountsService.fetchAccounts() {
            for account in accounts {
                accountsDropdown.addItem(withTitle: account.value.getName())

                if accountsService.currentlyUsedAccountID == account.value.getID() {
                    accountsDropdown.selectItem(withTitle: account.value.getName())
                }
            }
        }

        accountsDropdown.synchronizeTitleAndSelectedItem()
    }

    @IBAction func accountSelected(button _: NSPopUpButton) {
        updatePreferences()

        guard let name = accountsDropdown.selectedItem?.title else {
            return
        }

        if let account = accountsService.fetchAccount(byName: name) {
            accountsService.currentlyUsedAccountID = account.getID()
        }

        projects = Projects()
        tableView?.reloadData()
        populatePreferences()
        updateView()
    }

    @IBAction func projectTypeSelected(_: NSPopUpButtonCell) {
        updatePreferences()
        updateView()
    }

    @IBAction func refreshDelaySelected(_: NSPopUpButton) {
        updatePreferences()
    }

    @IBAction func quitClicked(_ sender: Any) {
        NSApp.terminate(sender)
    }

    @IBAction func refreshClicked(_: NSButton) {
        loadingIndicator(show: true)
        monitorService?.updateProjects()
    }

    override func shouldPerformSegue(withIdentifier identifier: NSStoryboardSegue.Identifier, sender _: Any?) -> Bool {
        if identifier == addAccountSegueKey {
            return true
        }

        if accountsService.accountsCount() != 0, identifier == editAccountSegueKey {
            return true
        }

        return false
    }

    override func prepare(for segue: NSStoryboardSegue, sender _: Any?) {
        credentialsViewController = (segue.destinationController as! CredentialsViewController)
        credentialsViewController?.delegate = self as CredentialsViewDelegate

        switch segue.identifier {
        case editAccountSegueKey:
            if let account = accountsService.fetchAccount(id: accountsService.currentlyUsedAccountID) {
                credentialsViewController?.currentAccount = account
            }

        default:
            credentialsViewController?.currentAccount = nil
        }
    }

    @IBAction func useTrafficLightClicked(_ sender: NSButton) {
        let ampel = AmpelClient()

        if var prefs = accountsService.getPreferences() {
            if sender.state == .on {
                sender.state = NSControl.StateValue.on
                prefs.useAmpel = true
            } else {
                sender.state = NSControl.StateValue.off
                ampel.switchAmpel(status: .allOff)
                prefs.useAmpel = false
            }

            accountsService.updateAccount(preferences: prefs)
        }
    }

    private func populatePreferences() {
        if let prefs = accountsService.getPreferences(accountID: accountsService.currentlyUsedAccountID) {
            let refreshItem = refreshDelayButton.item(withTitle: prefs.getFormattedRefreshDelay())
            refreshDelayButton.select(refreshItem)

            let projectTypeItem = projectTypeButton.item(withTitle: prefs.projectType)
            projectTypeButton.select(projectTypeItem)
        }
    }

    fileprivate func updatePreferences() {
        let delay = refreshDelayButton.selectedItem
        let projectType = projectTypeButton.selectedItem
        let prefs = Preferences(
            refreshDelay: delay!.title,
            projectType: projectType!.title
        )

        accountsService.updateAccount(preferences: prefs)
        monitorService?.intervalDidUpdate()
    }

    private func loadingIndicator(show: Bool) {
        switch show {
        case true:
            refreshButton?.isHidden = true
            loadingIndicator?.isHidden = false
            loadingIndicator?.startAnimation(nil)
        default:
            refreshButton?.isHidden = false
            loadingIndicator?.isHidden = true
            loadingIndicator?.stopAnimation(nil)
        }
    }

    private func registerEvents() {
        Bus<AppEvents>.register(self) { event in
            if case .networkError = event {
                self.loadingIndicator(show: false)
            }
        }
    }
}

extension PreferencesViewController: NSTableViewDelegate {
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        switch tableColumn?.identifier {
        case NSUserInterfaceItemIdentifier(rawValue: "firstColumn"):
            let cellIdentifier = NSUserInterfaceItemIdentifier(rawValue: "checkBoxCell")
            guard let cell = tableView.makeView(withIdentifier: cellIdentifier, owner: self) as? PreferencesTableCell else {
                return nil
            }

            guard projects != nil, projects!.count >= 0 else {
                cell.textField?.stringValue = "no data available"
                return cell
            }

            cell.projectName.stringValue = (projects?[row].displayName)!
            cell.checkBox.markedCheck(checked: (projects?[row].subscribed)!)
            cell.projectId = projects?[row].id
            cell.controller = self
            return cell

        default:
            log.error("unknown NSUserInterfaceItemIdentifier")
            return nil
        }
    }

    private func updateView() {
        if accountsService.accountsCount() > 0 {
            loadingIndicator(show: true)
            monitorService?.updateProjects()
            return
        }

        projects?.removeAll()
        tableView?.reloadData()
        Bus<AppEvents>.post(.accountsDidReset)
    }
}

extension PreferencesViewController: NSTableViewDataSource {
    func numberOfRows(in _: NSTableView) -> Int {
        projects?.count ?? 0
    }
}

extension PreferencesViewController: NSControlTextEditingDelegate {
    func controlTextDidChange(_: Notification) {
        updateView()
        initAccountsPopUp()
    }
}

extension PreferencesViewController: CredentialsViewDelegate {
    func credentialsDidUpdate() {
        projects = Projects()
        initAccountsPopUp()
        updateView()
    }
}
