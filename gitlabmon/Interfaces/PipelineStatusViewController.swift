import Cocoa

class PipelineStatusViewController: NSViewController {
    let accountService = AccountsRepository.shared
    @IBOutlet var statusTableView: NSTableView?
    @IBOutlet var projectCountLabel: NSTextField!
    @IBOutlet var updatedLabel: NSButton!
    @IBOutlet var scrollView: NSScrollView!
    @IBOutlet var versionLabel: NSTextField!

    weak var monitorService: MonitorService?

    var pipelines: Pipelines? {
        didSet {
            pipelines?.removeInvalid()
            DispatchQueue.main.async {
                self.statusTableView?.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        statusTableView?.delegate = self
        statusTableView?.dataSource = self as NSTableViewDataSource
        statusTableView?.backgroundColor = .lightGray
        statusTableView?.alphaValue = 1.0
        projectCountLabel.toolTip = "projects with pipelines"
        updatedLabel.toolTip = "click to refresh"
        versionLabel.stringValue = "v" + Bundle.getVersionAndBuild()
    }

    override func viewWillAppear() {
        super.viewWillAppear()
        if accountService.accountsCount() == 0 {
            pipelines = nil
            return
        }

        monitorService?.updateProjectsStatus()
        statusTableView?.reloadData()
    }

    @IBAction func refreshData(_: NSButton) {
        monitorService?.updateProjectsStatus()
    }

    private func setUpdated() {
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        updatedLabel.title = "↻ " + formatter.string(from: now)
    }
}

extension PipelineStatusViewController: NSTableViewDelegate {
    func tableView(_ tableView: NSTableView, viewFor _: NSTableColumn?, row: Int) -> NSView? {
        let tmpRow: NSTableRowView = tableView.rowView(atRow: row, makeIfNecessary: false)!

        if row % 2 == 0 {
            tmpRow.backgroundColor = .lightGray
        } else {
            tmpRow.backgroundColor = NSColor(calibratedRed: 0.75, green: 0.75, blue: 0.75, alpha: 1.0)
        }

        let cellIdentifier = NSUserInterfaceItemIdentifier(rawValue: "pipelineStatusCell")
        guard let cellView = tableView.makeView(withIdentifier: cellIdentifier, owner: self) as? PipelineStatusTableCell else { return nil }

        if let pipelines {
            if pipelines.count > 0 {
                cellView.populateCell(pipeline: pipelines[row])
                return cellView
            }
        }
        return cellView
    }
}

extension PipelineStatusViewController: NSTableViewDataSource {
    func numberOfRows(in _: NSTableView) -> Int {
        projectCountLabel.stringValue = "\(pipelines?.count ?? 0)"
        setUpdated()
        return pipelines?.count ?? 0
    }
}
