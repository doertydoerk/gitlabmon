import Alamofire
import Cocoa
import RealEventsBus

protocol CredentialsViewDelegate {
    func credentialsDidUpdate()
}

/*
 On creation of CredentialsViewController the currentAccountIndex value is set by the calling view controller.
 This value represents the index of the currently used account in the accounts array.

 - value is => 0: user wants to edit the currently used account credentials
 - value is < 0: user wants to add an editional account so we show an view with no values
 */
class CredentialsViewController: NSViewController {
    private var accountsService = AccountsRepository.shared
    var textfieldDidChange = false
    var currentAccount: GitlabAccount?
    var delegate: CredentialsViewDelegate?

    @IBOutlet var urlTextField: NSTextField!
    @IBOutlet var userIdTextField: NSTextField!
    @IBOutlet var tokenTextField: NSSecureTextField!

    @IBOutlet var doneButton: NSButton!
    @IBOutlet var deleteButton: NSButton!
    @IBOutlet var networkStatusButton: NSButton!
    @IBOutlet var loadingIndicator: NSProgressIndicator!

    override func viewDidLoad() {
        urlTextField.delegate = self
        userIdTextField.delegate = self
        tokenTextField.delegate = self
        registerEvents()
    }

    func setNetworkState(state: NetworkState) {
        switch state {
        case .credentialsValid:
            networkStatusButton.contentTintColor = .systemYellow
            networkStatusButton.toolTip = "valid credential format, no successful connection"
        case .successfulConnected:
            networkStatusButton.contentTintColor = .systemGreen
            networkStatusButton.toolTip = "successful connection"
        case .unknown:
            networkStatusButton.contentTintColor = .systemRed
            networkStatusButton.toolTip = "credentials invalid"
        }
    }

    override func viewWillAppear() {
        setNetworkState(state: .unknown)

        if currentAccount != nil {
            populateTextFields()
        } else {
            currentAccount = GitlabAccount(credentials: Credentials())
        }

        if isCredentialsValid() {
            setNetworkState(state: .credentialsValid)
            testConnection()
        }
    }

    deinit {
        Bus<AppEvents>.unregister(self)
    }

    @IBAction func doneButtonClicked(_: NSButton) {
        var url = urlTextField.stringValue
        if url == "" {
            url = "https://put-real-account-url.here"
        }

        currentAccount!.credentials!.url = url
        currentAccount!.credentials!.userId = userIdTextField.stringValue
        currentAccount!.credentials!.token = tokenTextField.stringValue
        accountsService.addAccount(account: currentAccount!)
        accountsService.currentlyUsedAccountID = currentAccount!.getID()
        delegate?.credentialsDidUpdate()
        dismiss(self)
    }

    @IBAction func deleteAccountClicked(_: NSButton) {
        accountsService.deleteAccount(id: currentAccount!.getID())

        setNewCurrentlyUsedAccount()
        delegate?.credentialsDidUpdate()
        dismiss(self)
    }

    @IBAction func networkButtonCLicked(_: NSButton) {
        testConnection()
    }

    private func setNewCurrentlyUsedAccount() {
        if accountsService.accountsCount() < 1 {
            accountsService.currentlyUsedAccountID = ""
            currentAccount = GitlabAccount(credentials: Credentials())
            return
        }

        currentAccount = GitlabAccount(credentials: Credentials())
        currentAccount = accountsService.fetchAccount(byIndex: 0)!
        accountsService.currentlyUsedAccountID = currentAccount!.getID()
    }

    private func populateTextFields() {
        setNetworkState(state: .unknown)

        urlTextField.stringValue = currentAccount!.credentials!.url
        userIdTextField.stringValue = currentAccount!.credentials!.userId
        tokenTextField.stringValue = currentAccount!.credentials!.token

        let allValid = Validator().allValid(
            url: currentAccount!.credentials!.url,
            userID: currentAccount!.credentials!.userId,
            token: currentAccount!.credentials!.token
        )

        if allValid {
            setNetworkState(state: .credentialsValid)
            testConnection()
        }
    }

    private func isCredentialsValid() -> Bool {
        Validator().allValid(
            url: urlTextField.stringValue,
            userID: userIdTextField.stringValue,
            token: tokenTextField.stringValue
        )
    }

    private func registerEvents() {
        Bus<AppEvents>.register(self) { event in
            self.loadingIndicator(show: false)

            if case let .networkConnected(req) = event {
                if let url = req.url {
                    if self.isResponseForTestRequest(url: url) {
                        self.setNetworkState(state: .successfulConnected)
                    }
                }
            }

            // check the error is for the creds in question as another reqq for different creds may have been returned, too
            if case let .networkError(req, _) = event {
                if let url = req.url {
                    if self.isResponseForTestRequest(url: url) {
                        self.setNetworkState(state: .credentialsValid)
                    }
                }
            }
        }
    }

    func isResponseForTestRequest(url: URL) -> Bool {
//        log.debug(url.absoluteString)
        let idString = "/" + userIdTextField.stringValue + "/"
        return (url.absoluteString.range(of: idString) != nil) && (url.absoluteString.range(of: urlTextField.stringValue) != nil)
    }

    private func loadingIndicator(show: Bool) {
        switch show {
        case true:
            networkStatusButton?.isHidden = true
            loadingIndicator?.isHidden = false
            loadingIndicator?.startAnimation(nil)
        default:
            networkStatusButton?.isHidden = false
            loadingIndicator?.isHidden = true
            loadingIndicator?.stopAnimation(nil)
        }
    }

    fileprivate func testConnection() {
        let creds = Credentials(
            url: urlTextField.stringValue,
            userId: userIdTextField.stringValue,
            token: tokenTextField.stringValue
        )

        Bus<AppEvents>.post(.testNetworkRequest(creds: creds))
    }
}

extension CredentialsViewController: NSTextFieldDelegate {
    func controlTextDidEndEditing(_: Notification) {
        if textfieldDidChange == false { return }

        if isCredentialsValid() {
            loadingIndicator(show: true)
            setNetworkState(state: .credentialsValid)
            testConnection()

            textfieldDidChange = false
            return
        }

        setNetworkState(state: .unknown)
    }

    func controlTextDidChange(_: Notification) {
        textfieldDidChange = true
    }
}
