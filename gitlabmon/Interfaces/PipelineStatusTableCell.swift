import Cocoa

class PipelineStatusTableCell: NSTableCellView {
    @IBOutlet var statusImage: NSImageView!
    @IBOutlet var agoLabel: NSTextField!
    @IBOutlet var projectLabel: NSTextField!
    @IBOutlet var branchLabel: NSTextField!
    var pipelineUrl: URL!

    override var isOpaque: Bool {
        true
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        alphaValue = 1.0
        projectLabel.textColor = NSColor(calibratedWhite: 0.0, alpha: 1.0)
        branchLabel.textColor = NSColor(calibratedWhite: 0.0, alpha: 1.0)
        agoLabel.textColor = NSColor(calibratedWhite: 0.0, alpha: 1.0)
    }

    @IBAction func cellButton(sender _: NSButton) {
        window!.close()
        NSWorkspace.shared.open(pipelineUrl)
    }

    func populateCell(pipeline: Pipeline) {
        switch pipeline.status {
        case .failed:
            statusImage!.image = NSImage(named: "fail")
        case .success:
            statusImage!.image = NSImage(named: "passed")
        case .cancelled:
            statusImage!.image = NSImage(named: "cancelled")
        case .running:
            statusImage!.image = NSImage(named: "wip")
        default:
            statusImage!.image = NSImage(named: "unknown")
        }

        projectLabel.stringValue = pipeline.getProjectName()
        agoLabel.stringValue = pipeline.timeSinceLastUpdate() + " ago"
        branchLabel.stringValue = "\(pipeline.getShortHash()) - \(pipeline.branch)"
        pipelineUrl = URL(string: pipeline.webUrl)!
    }

    func noData() {
        statusImage.image = nil
        projectLabel.stringValue = "no data available"
        agoLabel.stringValue = ""
        branchLabel.stringValue = ""
        pipelineUrl = nil
    }
}
