import Foundation

typealias Projects = [Project]

struct Project: Codable {
    let id: Int
    let displayName: String
    var subscribed: Bool

    static func newFrom(raw: [String: Any]) -> Project {
        Project(id: raw["id"] as! Int,
                displayName: raw["name_with_namespace"] as! String,
                subscribed: true)
    }
}

extension Projects {
    mutating func subscribeProject(projectId: Int) {
        updateSubscription(subscribe: true, projectId: projectId)
    }

    mutating func unsubscribeProject(projectId: Int) {
        updateSubscription(subscribe: false, projectId: projectId)
    }

    private mutating func updateSubscription(subscribe: Bool, projectId: Int) {
        for i in 0 ..< count {
            if self[i].id == projectId {
                let new = Project(id: projectId, displayName: self[i].displayName, subscribed: subscribe)
                replaceSubrange(Range(i ... i), with: Projects(arrayLiteral: new))
                break
            }
        }
    }

    mutating func update(newProjects: [Project]) -> [Project] {
        var updated = newProjects
        for (i, newProject) in updated.enumerated() {
            if isAlreadyKnown(new: newProject) {
                updated[i].subscribed = subscriptionStateForProject(id: updated[i].id)
            }
        }

        // sortBy(subscribed: true)
        return updated
    }

    private func isAlreadyKnown(new: Project) -> Bool {
        for old in self {
            if old.id == new.id {
                return true
            }
        }

        return false
    }

    private func subscriptionStateForProject(id: Int) -> Bool {
        for p in self {
            if p.id == id {
                return p.subscribed
            }
        }

        return false
    }

    mutating func sortBy(subscribed: Bool) {
        if subscribed {
            sort { $0.subscribed && !$1.subscribed }
        } else {
            sort { !$0.subscribed && $1.subscribed }
        }
    }

    func subscribedCount() -> Int {
        var count = 0
        for project in self {
            if project.subscribed {
                count = count + 1
            }
        }
        return count
    }

    func getSubscribedProjectsIds() -> [String] {
        var subscribed = [String]()
        for project in self {
            if project.subscribed {
                subscribed.append("\(project.id)")
            }
        }
        return subscribed
    }
}
