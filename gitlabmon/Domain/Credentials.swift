public struct Credentials: Codable {
    var url = ""
    var userId = ""
    var token = ""
}
