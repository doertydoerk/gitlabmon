import Foundation

typealias GitlabAccounts = [String: GitlabAccount]

struct GitlabAccount: Codable {
    var id: String
    var credentials: Credentials?
    var preferences: Preferences?
    var projects: Projects?

    init(credentials: Credentials) {
        id = UUID().uuidString
        self.credentials = credentials
        preferences = Preferences()
        projects = Projects()
    }

    // returns the display name for the account
    func getName() -> String {
        let url = URL(string: credentials!.url)
        return url?.host ?? ""
    }

    // return the credentials token which is used as key in the accounts dictionary to id the account
    func getID() -> String {
        id
    }

    func fetchProjects() -> Projects? {
        projects
    }

    func fetchCredentials() -> Credentials? {
        credentials
    }

    mutating func update(credentials: Credentials) {
        self.credentials = credentials
    }

    func fetchPreferences() -> Preferences {
        preferences!
    }

    mutating func update(preferences: Preferences) {
        self.preferences = preferences
    }

    mutating func update(projects: Projects) {
        self.projects = projects
    }
}
