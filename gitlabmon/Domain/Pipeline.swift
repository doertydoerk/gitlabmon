import Foundation

enum PipelineStatus {
    case success, failed, running, cancelled, unknown
}

typealias Pipelines = [Pipeline]

struct Pipeline: Hashable {
    let updatedAt: String
    let branch: String
    let status: PipelineStatus
    let webUrl: String
    let hash: String

    func hash(into hasher: inout Hasher) {
        hasher.combine(hash)
    }

    static func newFrom(raw: [String: Any]) -> Pipeline {
        var status: PipelineStatus
        switch raw["status"] as! String {
        case "success":
            status = .success
        case "failed":
            status = .failed
        case "running":
            status = .running
        case "canceled":
            status = .cancelled
        default:
            status = .unknown
        }

        return Pipeline(updatedAt: raw["updated_at"] as! String,
                        branch: raw["ref"] as! String,
                        status: status,
                        webUrl: raw["web_url"] as! String,
                        hash: raw["sha"] as! String)
    }

    func getProjectName() -> String {
        let url = URL(string: webUrl)!
        let components = url.pathComponents
        return projectNameFromComponents(components: components)
    }

    func projectNameFromComponents(components: [String]) -> String {
        var projectName = ""
        for item in components {
            if item.hasPrefix("/") {
                continue
            }

            if item.hasSuffix("-") {
                break
            }

            projectName += " / " + item
        }
        return String(projectName.dropFirst(3))
    }

    // Calculates the time elapsed since e.g. '2021-10-15T03:00:10.618Z' in abbreviated string format e.g. '16h'
    func timeSinceLastUpdate() -> String {
        Duration.fromTimestamp(timestamp: updatedAt)
    }

    func getShortHash() -> String {
        String(hash.prefix(8))
    }
}

extension Pipelines {
    func hasFailedPipeline() -> Bool {
        for p in self {
            if p.status == .failed {
                return true
            }
        }

        return false
    }

    func sortByAge() -> Pipelines {
        sorted { $0.updatedAt > $1.updatedAt }
    }

    mutating func removeInvalid() {
        for pipeline in self {
            if pipeline.status == .unknown {
                remove(at: firstIndex(of: pipeline)!)
            }
        }
    }
}
