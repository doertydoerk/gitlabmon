import Alamofire
import Foundation
import RealEventsBus

public enum AppEvents: Event {
    case accountsDidReset
    case networkError(req: URLRequest, error: AFError)
    case networkConnected(req: URLRequest)
    case testNetworkRequest(creds: Credentials)
}
