struct Preferences: Codable {
    var refreshDelay = "120"
    var projectType = ProjectType.owned
    var useAmpel = false

    func getRefreshDelay() -> String {
        refreshDelay.replacingOccurrences(of: " sec.", with: "")
    }

    func getFormattedRefreshDelay() -> String {
        refreshDelay
    }

    mutating func setRefreshDelay(delay: String) {
        refreshDelay = delay
    }
}
