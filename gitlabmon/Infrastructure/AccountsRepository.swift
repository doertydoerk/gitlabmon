import Foundation

enum ProjectType {
    static let owned = "owned"
    static let starred = "starred"
}

class AccountsRepository {
    private var defaults = UserDefaults.standard
    static let accountsKey = "accounts"
    static let currentAccountKey = "currentAccount"
    var currentlyUsedAccountID = ""

    // returns a singleton of DefaultAccountService
    static let shared = AccountsRepository()

    // used to inject defaults for testing so shared defaults don't get overwritten
    init(withUserDefault userDefaults: UserDefaults = UserDefaults.standard) {
        defaults = userDefaults

        if let currentAccountID = defaults.string(forKey: AccountsRepository.currentAccountKey) {
            currentlyUsedAccountID = currentAccountID
        }
    }

    func fetchAccounts() -> GitlabAccounts? {
        if let data = defaults.data(forKey: AccountsRepository.accountsKey), let accounts = try? JSONDecoder().decode(GitlabAccounts.self, from: data) {
            return accounts
        }

        return nil
    }

    func fetchAccount(id: String) -> GitlabAccount? {
        fetchAccounts()?[id]
    }

    func fetchAccount(byIndex index: Int) -> GitlabAccount? {
        fetchAccounts()?[index]?.value
    }

    func fetchAccount(byName name: String) -> GitlabAccount? {
        if let accounts = fetchAccounts() {
            for account in accounts.values {
                if name == account.getName() {
                    return account
                }
            }
        }

        return nil
    }

    func addAccount(account: GitlabAccount) {
        guard account.credentials != nil else {
            log.error("cannot save account with credentials == nil")
            return
        }

        if fetchAccounts() == nil {
            // if account don't exist because of e.g first time use, we need to create it to avoid nil
            persist(accounts: GitlabAccounts())
            addAccount(account: account)
        }

        if var accounts = fetchAccounts() {
            accounts[account.getID()] = account
            currentlyUsedAccountID = account.getID()
            persist(accounts: accounts)
        }
    }

    func resetDefaults() {
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }

    func getAccount(id: String) -> GitlabAccount? {
        fetchAccounts()?[id]
    }

    func deleteAccount(id: String) {
        if var accounts = fetchAccounts() {
            accounts.removeValue(forKey: id)
            persist(accounts: accounts)
        }
    }

    func accountsCount() -> Int {
        if let accounts = fetchAccounts() {
            return accounts.count
        }

        return 0
    }

    // attaches new preferences to the account with the given ID
    func updateAccount(id: String? = nil, preferences: Preferences) {
        var identifier = id
        if identifier == nil {
            identifier = currentlyUsedAccountID
        }

        if var accounts = fetchAccounts() {
            accounts[identifier!]?.preferences = preferences
            persist(accounts: accounts)
        }
    }

    // get the preferences for the account with the given ID
    func getPreferences(accountID id: String? = nil) -> Preferences? {
        var identifier = id
        if identifier == nil {
            identifier = currentlyUsedAccountID
        }

        if let account = fetchAccounts()?[identifier!] {
            return account.preferences
        }

        log.debug("account not found: \(identifier!)")
        return nil
    }

    // attaches new credentials to the account with the given ID
    func updateAccount(id: String? = nil, credentials: Credentials) {
        var identifier = id
        if identifier == nil {
            identifier = currentlyUsedAccountID
        }

        if var accounts = fetchAccounts() {
            accounts[identifier!]?.credentials = credentials
            persist(accounts: accounts)
        }
    }

    // get the credentials for the account with the given ID
    func getCredentials(accountID id: String? = nil) -> Credentials? {
        var identifier = id
        if identifier == nil {
            identifier = currentlyUsedAccountID
        }

        if let account = fetchAccount(id: identifier!) {
            return account.credentials
        }

        log.debug("account not found: \(identifier!)")
        return nil
    }

    // attaches new credentials to the account with the given ID
    func updateAccount(id: String? = nil, projects: Projects) {
        var identifier = id
        if identifier == nil {
            identifier = currentlyUsedAccountID
        }

        if var accounts = fetchAccounts() {
            accounts[identifier!]?.projects = projects
            persist(accounts: accounts)
        }
    }

    // get the credentials for the account with the given ID
    func getProjects(accountID id: String? = nil) -> Projects? {
        var identifier = id
        if identifier == nil {
            identifier = currentlyUsedAccountID
        }

        if let account = fetchAccounts()?[identifier!] {
            return account.projects
        }

        log.debug("account not found: \(identifier!)")
        return nil
    }

    private func persist(accounts: GitlabAccounts) {
        if let data = try? JSONEncoder().encode(accounts) {
            defaults.set(data, forKey: AccountsRepository.accountsKey)
            defaults.set(currentlyUsedAccountID, forKey: AccountsRepository.currentAccountKey)
        }
    }
}
