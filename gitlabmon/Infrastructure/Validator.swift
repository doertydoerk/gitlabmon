import Foundation

struct Validator {
    func allValid(url: String, userID: String, token _: String) -> Bool {
        if !isValidUrl(url) {
            log.warning("URL: invalid, must be e.g. http://example.com: \(url)")
            return false
        }

        if !isValidUserId(userID) {
            log.warning("user ID: invalid, must be an integer")
            return false
        }

        // TODO: allow for special characters
//        if !isValidToken(token) {
//            log.warning("token: invalid, must be alphanumeric")
//            return false
//        }

        return true
    }

    private func isValidUrl(_ urlString: String) -> Bool {
        let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format: "SELF MATCHES %@", argumentArray: [regEx])
        return predicate.evaluate(with: urlString)
    }

    private func isValidUserId(_ idString: String) -> Bool {
        let sevenDigitInteger = "^[1-9][0-9]*$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", argumentArray: [sevenDigitInteger])
        return predicate.evaluate(with: idString)
    }

    private func isValidToken(_ tokenString: String) -> Bool {
        let regEx = "^[a-zA-Z0-9]+$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", argumentArray: [regEx])
        return predicate.evaluate(with: tokenString)
    }
}
