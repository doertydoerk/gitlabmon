import Alamofire
import AlamofireNetworkActivityLogger
import Foundation
import RealEventsBus

enum NetworkState {
    case unknown
    case credentialsValid
    case successfulConnected
}

protocol CiClientDelegate {
    func pipelineDidUpdate(pipeline: Pipeline)
    func projectsDidUpdate(newProject: [Project])
}

protocol CiClient {
    func fetchPipelines(projectIDs: [String])
    func fetchProjects()
    func testConnection(creds: Credentials)
}

class MockClient: CiClient {
    func testConnection(creds _: Credentials) {
        log.error("not implemented")
    }

    var delegate: CiClientDelegate?

    func fetchPipelines(projectIDs _: [String]) {
        let url = "http_example.com/group_name/project_name"
        let time = "2021-10-15T03:00:10.618Z"
        let pipe1 = Pipeline(updatedAt: time, branch: "branch_1", status: .success, webUrl: url, hash: "123ABC")
        let pipe2 = Pipeline(updatedAt: time, branch: "branch_2", status: .cancelled, webUrl: url, hash: "123ABC")
        let pipe3 = Pipeline(updatedAt: time, branch: "branch_3", status: .failed, webUrl: url, hash: "123ABC")
        let pipe4 = Pipeline(updatedAt: time, branch: "branch_4", status: .running, webUrl: url, hash: "123ABC")
        let pipelines = [pipe1, pipe2, pipe3, pipe4]
        for i in 0 ... 3 {
            delegate?.pipelineDidUpdate(pipeline: pipelines[i])
        }
    }

    func fetchProjects() {
        let project1 = Project(id: 1, displayName: "foo", subscribed: true)
        let project2 = Project(id: 2, displayName: "bar", subscribed: true)
        let project3 = Project(id: 3, displayName: "baz", subscribed: true)
        delegate?.projectsDidUpdate(newProject: [project1, project2, project3])
    }
}

class GitlabClient {
    var delegate: CiClientDelegate?
    private let accountService = AccountsRepository.shared

    init() {
        NetworkActivityLogger.shared.level = .warn
        NetworkActivityLogger.shared.startLogging()
        registerEvents()
    }

    private func fetchPipelines(projectId: String) {
        guard let creds = accountService.getCredentials() else {
            log.error("no credentials to perform http request")
            return
        }

        let url = creds.url + "/api/v4/projects/" + projectId + "/pipelines"
        makePipelineRequest(url: url)
    }

    private func makePipelineRequest(url: String) {
        guard let creds = accountService.getCredentials() else {
            log.error("no credentials to perform http request")
            return
        }

        var originalReq: URLRequest?

        let headers = HTTPHeaders(["PRIVATE-TOKEN": creds.token, "PAGE": "1"])
        let params = ["per_page": "1"]
        _ = AF.request(url, method: .get, parameters: params, headers: headers)
            .onURLRequestCreation { req in
                originalReq = req
            }
            .validate(statusCode: 200 ..< 300)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case let .success(data):
                    Bus<AppEvents>.post(.networkConnected(req: originalReq!))
                    self.handlePipelineResponse(data: data)
                case let .failure(error):
                    Bus<AppEvents>.post(.networkError(req: originalReq!, error: error))
                    log.error(error)
                }
            }
    }

    private func makeProjectsRequest(url: String, headers: HTTPHeaders) {
        var originalReq: URLRequest?

        let params = ["per_page": "50", "simple": "true"]
        _ = AF.request(url, method: .get, parameters: params, headers: headers)
            .onURLRequestCreation { req in
                originalReq = req
            }
            .validate(statusCode: 200 ..< 300)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case let .success(data):
                    Bus<AppEvents>.post(.networkConnected(req: originalReq!))
                    self.handleProjectResponse(data: data)
                case let .failure(error):
                    Bus<AppEvents>.post(.networkError(req: originalReq!, error: error))
                    log.error(error)
                }
            }
    }

    private func handlePipelineResponse(data: Any) {
        if let rawPipelines = data as? [[String: Any]] {
            if rawPipelines.count > 0 {
                let pipeline = Pipeline.newFrom(raw: rawPipelines[0])
                delegate?.pipelineDidUpdate(pipeline: pipeline)
                return
            }

            delegate?.pipelineDidUpdate(pipeline: getDummyPipeline())
            return
        }

        delegate?.pipelineDidUpdate(pipeline: getDummyPipeline())
    }

    private func getDummyPipeline() -> Pipeline {
        Pipeline(updatedAt: Date().nowRfc3339(), branch: "n/a", status: .unknown, webUrl: "https://git.com/some%20project%20had%20no%20proper%20pipeline/-", hash: "n/a")
    }

    private func handleProjectResponse(data: Any) {
        var allProjects = Projects()
        if let rawProject = data as? [[String: Any]] {
            for rawProject in rawProject {
                let project = Project.newFrom(raw: rawProject)
                allProjects.append(project)
            }
        }

        if allProjects.count >= (data as? [[String: Any]])!.count {
            delegate?.projectsDidUpdate(newProject: allProjects)
        }
    }

    private func registerEvents() {
        Bus<AppEvents>.register(self) { event in
            if case let .testNetworkRequest(creds) = event {
                self.testConnection(creds: creds)
            }
        }
    }
}

extension GitlabClient: CiClient {
    func testConnection(creds: Credentials) {
        let url = creds.url + "/api/v4/users/" + creds.userId + "/projects"
        let headers = HTTPHeaders(["PRIVATE-TOKEN": creds.token, "PAGE": "1"])
        makeProjectsRequest(url: url, headers: headers)
    }

    func fetchPipelines(projectIDs: [String]) {
        for projectId in projectIDs {
            fetchPipelines(projectId: projectId)
        }
    }

    func fetchProjects() {
        guard let creds = accountService.getCredentials() else {
            log.error("no credentials to perform http request")
            return
        }

        var url = creds.url + "/api/v4/users/" + creds.userId

        let projectType = accountService.getPreferences()?.projectType
        switch projectType {
        case ProjectType.starred:
            url += "/starred_projects"
        default:
            url += "/projects"
        }

        let headers = HTTPHeaders(["PRIVATE-TOKEN": creds.token, "PAGE": "1"])
        makeProjectsRequest(url: url, headers: headers)
    }
}

extension String {
    func get(from: String, to: String) -> String? {
        (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom ..< endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom ..< substringTo])
            }
        }
    }
}
