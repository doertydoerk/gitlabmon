import Foundation

struct AmpelClient {
    enum AmpelClientError: Error {
        case unexpectedHttpStaus(code: Int)
    }

    enum AmpelStatus {
        case red, green, allOn, allOff
    }

    private let accountService = AccountsRepository.shared
    private let baseUrl = "http://192.168.178.40"
    private let redLight = "/manually/red/"
    private let greenLight = "/manually/green/"
    private let on = "on"
    private let off = "off"

    func switchAmpel(status: AmpelStatus) {
        guard let useAmpel = accountService.getPreferences()?.useAmpel else {
            return
        }

        if useAmpel {
            switch status {
            case .red:
                getRequest(url: baseUrl + greenLight + off)
                getRequest(url: baseUrl + redLight + on)
            case .green:
                getRequest(url: baseUrl + redLight + off)
                getRequest(url: baseUrl + greenLight + on)
            case .allOn:
                getRequest(url: baseUrl + greenLight + on)
                getRequest(url: baseUrl + redLight + on)
            default:
                getRequest(url: baseUrl + greenLight + off)
                getRequest(url: baseUrl + redLight + off)
            }
        }
    }

    private func getRequest(url: String) {
        let url = URL(string: url)
        let session = URLSession(configuration: .ephemeral)
        let task = session.dataTask(with: url!) { _, response, error in
            if error != nil {
                log.warning(error!.localizedDescription)
                return
            }

            if let httpResponse = response as? HTTPURLResponse {
                if !(httpResponse.statusCode >= 200 && httpResponse.statusCode <= 299) {
                    let err = AmpelClientError.unexpectedHttpStaus(code: httpResponse.statusCode)
                    log.warning(err.localizedDescription)
                    return
                }
            }
        }

        task.resume()
        log.debug("sending request: \(String(describing: task.currentRequest!))")
    }
}
