import Cocoa

protocol MonitorServiceDelegate {
    func pipelinesDidUpdate(pipelines: Pipelines)
    func projectsDidUpdate(projects: [Project])
}

class MonitorService {
    private var defaultInterval = "120"
    private var ciClient: CiClient?
    private var timer: RepeatingTimer?
    private var tmpPipelines: [Pipeline] = []
    private let ampel = AmpelClient()
    private let accountsService = AccountsRepository.shared
    var delegate: MonitorServiceDelegate?

    init(ciClient: CiClient) {
        // cannot use self.accountsSerivce here as does not exist yet
        let interval = Double(AccountsRepository.shared.getPreferences()?.getRefreshDelay() ?? defaultInterval)
        newTimer(interval: interval!)

        // (ciClient as? MockClient)?.delegate = self
        (ciClient as? GitlabClient)?.delegate = self

        self.ciClient = ciClient
        updateProjects()
        updateProjectsStatus()
    }

    func newTimer(interval: Double) {
        timer = nil
        timer = RepeatingTimer(timeInterval: interval)
        timer?.eventHandler = updateProjectsStatus
        timer?.resume()
    }

    func intervalDidUpdate() {
        let interval = accountsService.getPreferences()?.getRefreshDelay() ?? defaultInterval
        newTimer(interval: Double(interval)!)
    }

    func updateProjects() {
        ciClient?.fetchProjects()
    }

    func updateProjectsStatus() {
        if let projects = accountsService.getProjects() {
            if projects.count == 0 {
                log.debug("no projects found")
                delegate?.pipelinesDidUpdate(pipelines: Pipelines())
                ampel.switchAmpel(status: .allOff)
                return
            }

            tmpPipelines.removeAll()
            let subscribed = projects.getSubscribedProjectsIds()
            if subscribed.count <= 0 {
                delegate?.pipelinesDidUpdate(pipelines: Pipelines())
            }

            ciClient?.fetchPipelines(projectIDs: subscribed)
        }
    }
}

extension MonitorService: CiClientDelegate {
    func projectsDidUpdate(newProject: [Project]) {
        if var knownProjects = accountsService.getProjects() {
            let updated = knownProjects.update(newProjects: newProject)
            accountsService.updateAccount(projects: updated)
            delegate?.projectsDidUpdate(projects: updated)
        }
    }

    func pipelineDidUpdate(pipeline: Pipeline) {
        tmpPipelines.append(pipeline)
        if tmpPipelines.count != accountsService.getProjects()?.subscribedCount() {
            return
        }

        delegate?.pipelinesDidUpdate(pipelines: tmpPipelines)
    }
}
