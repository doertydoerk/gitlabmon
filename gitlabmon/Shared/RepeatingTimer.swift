import Foundation

public class RepeatingTimer {
    enum State {
        case suspended
        case resumed
    }

    let timeInterval: TimeInterval
    var state: State = .suspended

    lazy var timer: DispatchSourceTimer = {
        let t = DispatchSource.makeTimerSource()
        t.schedule(deadline: .now() + self.timeInterval, repeating: self.timeInterval)
        t.setEventHandler(handler: { [weak self] in
            self?.eventHandler?()
        })
        return t
    }()

    public var eventHandler: (() -> Void)?

    public init(timeInterval: TimeInterval) {
        self.timeInterval = timeInterval
    }

    public func resume() {
        if state == .resumed {
            return
        }

        state = .resumed
        timer.resume()
    }

    public func suspend() {
        if state == .suspended {
            return
        }
        state = .suspended
        timer.suspend()
    }

    deinit {
        timer.setEventHandler {}
        timer.cancel()
        /*
         If the timer is suspended, calling cancel without resuming
         triggers a crash. This is documented here
         https://forums.developer.apple.com/thread/15902
         */
        resume()
        eventHandler = nil
    }
}
