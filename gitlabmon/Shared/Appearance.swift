import AppKit

struct AppearanceHelper {
    static func isLight() -> Bool {
        let appearance = NSApplication.shared.effectiveAppearance
        return appearance.bestMatch(from: [.aqua, .darkAqua]) == .aqua
    }
}
