import Foundation

extension Dictionary {
    mutating func merge(dict: [Key: Value]) {
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }

    subscript(i: Int) -> (key: Key, value: Value)? {
        if count > 0 {
            return self[index(startIndex, offsetBy: i)]
        }

        return nil
    }
}

extension Dictionary where Key == String, Value: Any {
    func object<T: Decodable>() -> T? {
        if let data = try? JSONSerialization.data(withJSONObject: self, options: []) {
            return try? JSONDecoder().decode(T.self, from: data)
        } else {
            return nil
        }
    }
}

extension UserDefaults {
    func valueExists(forKey key: String) -> Bool {
        object(forKey: key) != nil
    }
}

extension Date {
    func nowRfc3339() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return dateFormatter.string(from: self)
    }
}

extension Bundle {
    var releaseNumber: String {
        infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }

    var buildNumber: String {
        infoDictionary?["CFBundleVersion"] as? String ?? ""
    }

    static func getVersionAndBuild() -> String {
        Bundle.main.releaseNumber + "-" + Bundle.main.buildNumber
    }
}
