import Foundation

struct Duration {
    static let oneSecond = 1
    static let oneMinute = oneSecond * 60
    static let oneHour = oneMinute * 60
    static let oneDay = oneHour * 24
    static let oneWeek = oneDay * 7
    static let oneMonth = oneWeek * 4
    static let oneYear = oneMonth * 12

    static func fromTimestamp(timestamp: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

        guard let date = dateFormatter.date(from: timestamp) else {
            return ""
        }
        // let duration = (date?.timeIntervalSinceNow)

        let now = Date()
        let durationSeconds = Int(now.timeIntervalSince(date))

        return secondsToFormatted(seconds: durationSeconds)
    }

    static func secondsToFormatted(seconds: Int) -> String {
        if seconds < 60 {
            return "< 1m"
        }

        if seconds < oneHour {
            return "\(Int(seconds / oneMinute))m"
        }

        if seconds < oneDay {
            return "\(Int(seconds / oneHour))h"
        }

        if seconds < oneWeek {
            return "\(Int(seconds / oneDay))d"
        }

        if seconds < oneMonth {
            return "\(Int(seconds / oneWeek))w"
        }

        if seconds < oneYear {
            return "\(Int(seconds / oneMonth))m"
        }

        return "> 1y"
    }
}
