

[![pipeline status](https://gitlab.com/doertydoerk/gitlabmon/badges/master/pipeline.svg)](https://gitlab.com/doertydoerk/gitlabmon/commits/master)	[![coverage report](https://gitlab.com/doertydoerk/gitlabmon/badges/master/coverage.svg)](https://gitlab.com/doertydoerk/gitlabmon/commits/master)

# Gitlab Monitor

A macOS monitor for keeping track of the pipeline state of your projects.

<table align="center"><tr><td align="center" width="9999">
<img src="/resources/screenshot1.png" align="center" width="400" alt="screenshot">
<br>
<img src="/resources/screenshot2.png" align="center" width="400" alt="screenshot">

</td></tr></table>

Right click opens the preferences view. GitLab Monitor supports multiple GitLab accounts. Enter a new account. If the credentials are generally valid, the network indicator will turn yellow. If a successful connection has been made using these, the network indicator will turn green. At this point, all relevant GitLab projects will be listed. By default, they'll be all checked for providing status. You may choose between owned and starred projects.

The pipeline status indicator (menu bar icon) will turn green when the last pipeline of every monitored repo was successful. Otherwise, it will show red. For this to be determined, all pipelines that came back running or canceled are ignored until either a successful or failed pipelines was found.

In other words, the last non-grey (running or canceled) pipeline becomes the pipeline that determines the status of the repo in question.

## Traffic Light Button (Ampel Button)
GitlabMon was originally built to be used alongside the [Ampel-V2 Project](https://gitlab.com/doertydoerk/ampel-v2). The traffic light is turned off by default. Turning it on  will update the actual Ampel on every refresh to reflect the overall status across all pipelines. Having it turned on with no Ampel available obviously won't have any effect other than to cause redundant logging.

## Binary
Get the notarized binary [here](https://gitlab.com/doertydoerk/gitlabmon/-/releases).

## Known Issues
When changing between owned and shared projects in the preferences view, the checkbox state will get lost. So, you may have to check/uncheck the box again. This is not a bug really, but is how the model currently works. As a workaround, you can star all your owned projects and not use the starred option at all. I am probably not going to fix/change this, since it requires quite a change in the model and this workaround fully mitigates the problem.

## Credits
- [standard-version](https://github.com/doertydoerk/standard-version)
- [swiftformat](https://github.com/nicklockwood/SwiftFormat)
- [create-dmg](https://github.com/sindresorhus/create-dmg)
- [Alamofire](https://github.com/Alamofire/Alamofire)
- [RealEventsBus](https://github.com/malcommac/RealEventsBus.git)
- [SwiftyBeaver](https://github.com/SwiftyBeaver/SwiftyBeaver)

The app logo is based on the official git logo which is under [Creative Commons license](https://git-scm.com/downloads/logos).
